// C помощью prompt спросить как зовут пользователя: 'What is your name?'
// С помощью alert вывести "Hello, Alex! How are you?"
// где Alex это то что ввел пользователь

const getUserNameHandler = () => {
    const userName = prompt('What is your name?');

    if(userName === null) {
        return alert('User closed the form');
    }

    if(userName.length <= 1) {
        alert('Incorrect value. Try again');
        return getUserNameHandler();
    }

    return alert(`Hello, ${userName}! How are you?`);
}

getUserNameHandler();
